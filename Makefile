SHELL    := /usr/bin/bash
NAME      = # target binary name
SRC_DIR   = ./src
LDFLAGS   = -tags netgo -installsuffix netgo -ldflags '-w -s -extldflags "-static"' -buildmode=pie
GO_FILES := $(shell du -a ${SRC_DIR} | grep .go | awk '{print $$2}')

default: build

.PHONY: build
build:
	go build ${LDFLAGS} -o ${NAME} ${SRC_DIR}

.PHONY: run
run: build
	./${NAME}

.PHONY: test
test:
	$(info Serching Go files...)
	$(if ${GO_FILES},, \
		$(error Can not find Go file in ${SRC_DIR}) \
	)
	$(info Checking Go files...)
	gofmt -l ${GO_FILES}
	go vet ${SRC_DIR}/...
